from django.core.exceptions import ObjectDoesNotExist
from rest_framework import permissions
from rest_framework.views import APIView
from authentication.serializers import AuthTokenSerializer, ResetPasswordSerializer
from rest_framework.response import Response
from rest_framework.authtoken.models import Token


class LoginView(APIView):
    permission_classes = ()
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})


class ResetPasswordView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ResetPasswordSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        password = serializer.validated_data['new_password2']
        user = request.user
        user.set_password(password)
        user.save()
        return Response({'status': 'Contraseña cambiada.'})


class LogoutView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        try:
            request.user.auth_token.delete()
        except (AttributeError, ObjectDoesNotExist):
            pass

        return Response({'status': 'Éxitosamente deslogueado.'})
