from django.urls import path
from authentication.views import LoginView, ResetPasswordView, LogoutView

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('reset-password/', ResetPasswordView.as_view(), name='reset-password'),
    path('logout/', LogoutView.as_view(), name='logout')
]
