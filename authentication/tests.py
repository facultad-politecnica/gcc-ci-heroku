from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class UserLoginAPIViewTestCase(APITestCase):
    url_login = reverse('login')
    url_reset_pass = reverse('reset-password')
    url_logout = reverse('logout')

    def setUp(self):
        self.username = 'test'
        self.email = 'test@test.com'
        self.password = 'password123'
        self.user = User.objects.create_user(self.username, self.email, self.password)

    def test_authentication_without_password(self):
        response = self.client.post(self.url_login, {'username': 'username'})
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_authentication_with_wrong_password(self):
        response = self.client.post(self.url_login, {'username': self.username, 'password': 'wrongpassword'})
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_authentication_with_valid_data(self):
        response = self.client.post(self.url_login, {'username': self.username, 'password': self.password})
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_reset_password_no_match(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.post(
            self.url_reset_pass, {'new_password1': 'testpassword123', 'new_password2': 'testpassword'}
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_reset_password_with_success(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.post(
            self.url_reset_pass, {'new_password1': 'testpassword', 'new_password2': 'testpassword'}
        )
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_logout(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.url_logout)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(False, hasattr(self.user, 'auth_token'))
